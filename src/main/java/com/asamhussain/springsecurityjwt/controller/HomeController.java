package com.asamhussain.springsecurityjwt.controller;

import com.asamhussain.springsecurityjwt.Services.MyUserDetailsService;
import com.asamhussain.springsecurityjwt.models.AuthenticationRequest;
import com.asamhussain.springsecurityjwt.models.AuthenticationResponse;
import com.asamhussain.springsecurityjwt.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @RequestMapping("/")
    public String getMessage(){
        return "You allowed";
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getUsername(),
                            authenticationRequest.getPassword()
                    )
            );
        }
        //for the wrong password
        catch (BadCredentialsException e){
            throw new Exception("Invalid username or password", e);
        }

        //getting user details
        final UserDetails userDetails = myUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        //creating token
        final String jwt = jwtUtil.generateToken(userDetails);

        //return generated JWT token as response
        return ResponseEntity.ok(new AuthenticationResponse(jwt));

    }

}
